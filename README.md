# Red Gato

No uses más la internet para pasarle algo a alguien que tenes al lado.

![Ventana principal](img/redgato.png)
> una interface usuario de `netcat`

## Instalación

<pre>
cd /tmp
wget https://raw.github.com/b4zz4/RedGato/master/redgato
bash redgato -u
</pre>

## Actualización

<pre>
redgato -u
</pre>

## Que haceres

- Separar el cliente de la gui
   - Versión para la terminal
- Mensajería con torchat
- Todo sobre tor
